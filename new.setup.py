from setuptools import setup, find_packages, Extension
import glob

INCDIRS=['.']

setup(

    name	    = "Gtools",
    version	    = "4.0.0",
    author	    = "Revaz Yves",
    author_email    = "yves.revaz@epfl.ch",
    url 	    = "http://obswww.unige.ch/~revaz/Gtools",
    description     = "gadget toolbox",

    packages = find_packages(),


    ext_modules     = [Extension("Gtools.cgtools", 
    				 ["src/cgtools/cgtools.c"],
    				 include_dirs=INCDIRS)
    			       ],   
	
    scripts = glob.glob('scripts/*'),
	
					   
    #install_requires = ['numpy>=1.0.3'],
    #install_requires = ['matplotlib>=0.91.2'],
    dependency_links = [
        "http://obswww.unige.ch/~revaz/PyPI/"
    ]
    
    )
