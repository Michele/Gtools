#!/usr/bin/env python

from sys import argv
import os
import glob

#from distutils.sysconfig import *
from distutils.core import setup, Extension
from distutils.command.install_data import install_data

import numpy

long_description = """\
This module provides lots of tools used
to deal with Gadget models.
"""

package_dir = {} # used in case of dev option

dev = False

dev_arg = "--dev"
if dev_arg in argv:
  dev = True
  argv.remove(dev_arg)

INCDIRS=['.']
SCRIPTS =           glob.glob('Gtools/gscripts/*')
SCRIPTS = SCRIPTS + glob.glob('Gtools/pscripts/*')
if dev:
  SCRIPTS = SCRIPTS + glob.glob("Gtools/dev/gscripts/*")
  SCRIPTS = SCRIPTS + glob.glob("Gtools/dev/pscripts/*")

##############################################################
# variables for numpy
##############################################################
INCDIRS.append(numpy.get_include())

class Gtools_install_data (install_data):
  def finalize_options (self):
    if self.install_dir is None:
      install_lib = self.get_finalized_command('install_lib')
      self.warn_dir = 0
      self.install_dir = os.path.normpath(os.path.join(install_lib.install_dir,"Gtools"))

packages = ['Gtools','Gtools.pscripts','Gtools.gscripts']

if dev:
  
  packages.extend([
    "Gtools.dev.pscripts",
    "Gtools.dev.gscripts"
  ])

  package_dir["Gtools.pscripts"] = "Gtools/dev/pscripts"
  package_dir["Gtools.gscripts"] = "Gtools/dev/gscripts"

setup 	(       name	       	= "Gtools",
       		version         = "4.0.0",
       		author	      	= "Revaz Yves",
       		author_email    = "yves.revaz@obs.unige.ch",
		url 		= "http://obswww.unige.ch/~revaz",
       		description     = "Gtools module",
		
		packages 	= packages,

                package_dir = package_dir,
		
		cmdclass = {'install_data': Gtools_install_data},
		
                ext_modules 	= [Extension("Gtools.cgtools", 
				             ["src/cgtools/cgtools.c"],
					     include_dirs=INCDIRS)
					   ],
				   
                scripts = SCRIPTS			    
 	)
		
