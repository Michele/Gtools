#!/home/revaz/local/bin/python
'''
Plot density of the model as a function of radius
Estimate density from paticles mass, assuming a sperical
density distribution.

Yves Revaz
ven avr 14 16:06:59 CEST 2006
'''


from numarray import *
from pNbody import *
import SM
import string
import sys
import os

from libjeans import *

from pNbody.libutil import histogram

from optparse import OptionParser
from Gtools import *




def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser = add_postscript_options(parser)
  parser = add_color_options(parser)
  parser = add_limits_options(parser)
  parser = add_units_options(parser)
  parser = add_log_options(parser)
  parser = add_histogram_options(parser)
  parser = add_reduc_options(parser)

  parser.add_option("-t",
		   action="store", 
		   dest="ftype",
		   type="string",
		   default = None,		   
		   help="type of the file",	 
		   metavar=" TYPE")    

  parser.add_option("--rmax",
		   action="store", 
		   dest="rmax",
		   type="float",
		   default = 50,		   
		   help="max radius",	 
		   metavar=" FLOAT")    

  parser.add_option("--nb",
                    action="store", 
		    dest="nb", 
		    type="int",
		    default=25,
                    help="number of bins")
		    
  parser.add_option("--cgs",
  		    action="store_true", 
  		    dest="cgs",
		    default = 0,		    
  		    help="invert into cgs units")	

		     		    
		    		    		    
  (options, args) = parser.parse_args()

  if options.colors!=None:
    exec("options.colors = array([%s])"%(options.colors)) 

        
  if len(args) == 0:
    print "you must specify a filename"
    sys.exit(0)
    
  files = args
  
  return files,options



#############################
# graph
#############################

# get options
files,options = parse_options()
ps = options.ps
col = options.colors
xmin = options.xmin
xmax = options.xmax
ymin = options.ymin
ymax = options.ymax
log = options.log
histogram = options.histogram
reduc = options.reduc

cgs  = options.cgs
ftype = options.ftype
rmax = options.rmax
nb = options.nb

if cgs:
  units = get_units_options(options)



#######################################
# open sm
#######################################

g = Graph_Init(ps)
Graph_SetDefaultsGraphSettings(g)
colors = Graph_SetColorsForFiles(files,col)


#######################################
# LOOP
#######################################

# read files
for file in files:
				     		

  nbody = Nbody(file,ftype=ftype)	
  # nbody.cmcenter()

  if reduc!= None:
    nbody = nbody.reduc(reduc)  

  x,y = nbody.mdens(nb=nb,rm=rmax) 
  
  # convert into cgs
  if cgs:
    x = Length2cgs(x,units)
    y = Density2cgs(y,units)
    
  
  # use log
  if log != None:
    x,y = Graph_UseLog(x,y,log)
   

  if file == files[0]:
    xmin,xmax,ymin,ymax = Graph_SetLimits(g,xmin,xmax,ymin,ymax,x,y)
    Graph_DrawBox(g,xmin,xmax,ymin,ymax,log)
    			   
  
  g.ctype(colors[file])	

  # plot points	
  if histogram:
    xb,yb = Graph_MakeHistrogram(g,xmin,xmax,ymin,ymax,x,y) 
    g.connect(xb,yb) 
  else:
    g.points(x,y)      


g.ctype(0)
if   log == 'xy' or log == 'yx':
  g.xlabel('log R')
  g.ylabel('log Density')
elif log == 'x':
  g.xlabel('log R')
  g.ylabel('Density')
elif log == 'y':  
  g.xlabel('R')
  g.ylabel('log Density')
else:
  g.xlabel('R')
  g.ylabel('Density')


# -- end ---
Graph_End(g,ps)












