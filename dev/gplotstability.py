#!/usr/bin/env python
'''
Extract and plot energy and mass values contained in the
output Gadget file called by default "energy.txt".

Yves Revaz
ven jun  9 10:43:59 CEST 2006
'''

from numpy import *
from pNbody import *
import string
import sys
import os
import copy as docopy

from pNbody.libutil import histogram
from pNbody import libgrid

from optparse import OptionParser
from Gtools import *
from Gtools import io

import Ptools as pt


def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser = pt.add_postscript_options(parser)
  parser = pt.add_limits_options(parser)
  parser = pt.add_log_options(parser)
  
		      
  (options, args) = parser.parse_args()

    
  if len(args) == 0:
    print "you must specify a filename"
    sys.exit(0)
    
  files = args
  
  return files,options




#######################################
# MakePlot
#######################################


def MakePlot(dirs,opt):

  # some inits
  palette = pt.GetPalette()
  colors = pt.SetColorsForFiles(files,palette)
  labels = []




  #######################################
  # LOOP
  #######################################

  # read files
  for file in files:
  						  

    ######################################
    # open file
    ######################################


    stats = pt.io.read_hdf5(file)


    ###################################
    # now, plot
    ###################################

    color = colors[file]

    pt.subplot(3,1,1)
    x = stats['R']
    y = stats['Q']
    pt.plot(x,y,c=color)
    pt.plot(x,ones(len(x))*1,'r--')

    pt.subplot(3,1,2)
    x = stats['R']
    y = stats['X']
    pt.plot(x,y,c=color)    
    pt.plot(x,ones(len(x))*3,'r--')

    pt.subplot(3,1,3)
    x = stats['R']
    y = stats['A']
    pt.plot(x,y,c=color) 
    pt.plot(x,ones(len(x))*0.3,'r--') 
    
    xmin,xmax,ymin,ymax = pt.SetLimits(opt.xmin,opt.xmax,opt.ymin,opt.ymax,x,y,opt.log)
     
  
  
  
  
  pt.subplot(3,1,1)  
  pt.axis([xmin,xmax,0,5])
  pt.xlabel(r'$\rm{Radius}$')
  pt.ylabel(r'$\rm{Q}$')
  
  pt.subplot(3,1,2)  
  pt.axis([xmin,xmax,0,5])  
  pt.xlabel(r'$\rm{Radius}$')
  pt.ylabel(r'$\rm{X}$')
    
  pt.subplot(3,1,3)  
  pt.axis([xmin,xmax,0,1])   
  pt.xlabel(r'$\rm{Radius}$')
  pt.ylabel(r'$\rm{A}$')   

  pt.show()







if __name__ == '__main__':
  files,opt = parse_options()
  pt.InitPlot(files,opt)
  #pt.figure(figsize=(8*2,6*2))
  #pt.figure(dpi=10)
  pt.pcolors
  #fig = pt.gcf()
  #fig.subplots_adjust(left=0.1)
  #fig.subplots_adjust(right=1)
  #fig.subplots_adjust(bottom=0.12)
  #fig.subplots_adjust(top=0.95)
  #fig.subplots_adjust(wspace=0.25)
  #fig.subplots_adjust(hspace=0.02)  
  
  
  MakePlot(files,opt)
  pt.EndPlot(files,opt)
  

