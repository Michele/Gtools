#! /usr/bin/env python
"""
@package   GTools
@file      galaxyClass.py
@brief     Definition of a list of galaxy and a merger tree
@copyright GPLv3
@author    Loic Hausammann <loic_hausammann@hotmail.com>
@section COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL
"""
import numpy as np

from copy import deepcopy
from pNbody import Nbody

########### massPerType ###################
def massPerType(nb):
############################################
    """ Compute mass per particles type
    """
    part_type = 6
    mass = [0.]*part_type
    for i in range(part_type):
        mass[i] = nb.select(i).mass_tot
    return mass

########### compareNbody ###################
def compareNbody(nb1, nb2, ratio=0.8):
############################################
    """ Compare the indices of two Nbody obejcts.
    Returns True if at least ratio % particles are shared (else False).
    :param Nbody nb1: First Nbody object
    :param Nbody nb2: Second Nbody object
    :param float ratio: Ratio required to return True
    :returns: bool
    """
    N = np.sum(nb1.npart)
    # select particles from nb2 corresponding to nb1
    nb1 = nb1.selectp(nb2.num)
    # check if at least ${ratio}% of the particles are present
    if np.sum(nb1.npart) > ratio*N:
        return True
    else:
        return False

############################################
#             GalPosition                  #
############################################
class GalPosition:
    """
    Object containing all the main informations of a set of galaxies.
    The main information saved are the position, velocity and radius.
    """
############# __init__ #####################
    def __init__(self, atime, step=None):
############################################
        """
        Set all the array to 0 and copy atime
        :param str dir_data: Name of the data directory
        :param float atime: time of the snapshot
        :param str step: Time step
        """
        self.position = []
        self.mass = []
        self.index = []
        self.velocity = []
        self.radius = []
        self.atime = atime
        self.N = 0
        self.step = step
        # max(N) of all the previous timestep
        self.Nmax = 0

############ __str__ #######################
    def __str__(self):
############################################
        """
        used in order to print the object
        """
        s = "Step: %s \n" % self.step
        s += "atime: %g\n" % self.atime
        s += "N: %i\n\n" % self.N
        for i in range(self.N):
            s += "Pos: " + str(self.position[i]) + "\n"
            s += "Vel: " + str(self.velocity[i]) + "\n"
            s += "Rad: %g\n" % self.radius[i]
            s += "Mass: %g\n" % self.rmass[i]
            s += "Ind: %i\n\n" % self.index[i]
        return s

############# add ##########################
    def add(self, position, velocity, radius, mass):
############################################
        """ Add a new galaxy to the list
        :param [x,y,z] position: Position of the new galaxy
        :param [vx,vy,vz] velocity: Velocity of the new galaxy
        :param float radius: Radius of the new galaxy
        :param [] mass: Mass of each particles type
        """
        self.position.append(position)
        self.velocity.append(velocity)
        self.index.append(self.N)
        self.radius.append(radius)
        self.mass.append(mass)
        self.N += 1

############### predictPosition ############
    def predictPosition(self, next_atime=None):
############################################
        """ Predict the next position.
        if next_atime is given return x+v*t, otherwise return x
        :param float next_atime: Time of the next snapshot
        :returns: [[x,y,z]_i] Predicted position at next time step
        """
        pos = np.array(self.position)
        if next_atime is None:
            return pos
        dt = next_atime - self.atime
        vel = np.array(self.velocity)
        return pos + vel*dt

############ setIndices ####################
    def setIndices(self, prevGalPos, data_dir):
############################################
        """ Set the indices of the last time step from previous snapshots.
        First compare the position in order to find the best candidate,
        then check the particles indices.
        :param GalPos prevGalPos: Data from previous time step.
        :param str data_dir: Data directory
        """
        # predict position of the previous snapshot at current time step
        pos = prevGalPos.predictPosition(self.atime)
        # find index from previous time step
        for i in range(self.N):
            # test if index found
            test = True
            # open current galaxy
            nb = Nbody(data_dir + "/tmp"+str(i), ftype="gadget")
            # copy position of previous galaxies at current time step
            tmp = deepcopy(pos)
            # while index not found and possible candidates still exists
            while (test and tmp.shape[0] > 0):
                # distance between current galaxy and previous at
                # current time step
                dist2 = np.sum((tmp-self.position[i])**2, axis=1)

                # find closest galaxies
                ind = dist2.argmin()
                # open closest galaxy (from previous time step)
                nb_prev = Nbody(
                    data_dir + "/sub" + str(prevGalPos.index[ind]) +
                    "/snapshot_"+prevGalPos.step,
                    ftype="gadget")

                # compare if two nbody contain the same particles
                if compareNbody(nb_prev, nb):
                    # set to false in order to leave the while loop
                    test = False
                    # set index
                    self.index[i] = prevGalPos.index[ind]
                # if not found
                if test:
                    # delete galaxy tested from tmp
                    tmp = np.delete(tmp, ind, axis=0)
            # if not found
            if test:
                # set index to max of all the time step
                self.index[i] = prevGalPos.Nmax + 1
        # set the new max index of all the time step
        self.Nmax = max(np.array(self.index).max(), prevGalPos.Nmax)


############################################
#                 GalTree                  #
############################################
class GalTree():
    """
    Object containing a set of GalPos (representing the time evolution
    of the galaxies.
    """
############ __init __ #####################
    def __init__(self):
############################################
        self.galpos = []
        self.N = 0
    
############# add ##########################
    def add(self, galpos):
############################################
        self.galpos.append(galpos)
        self.N += 1

############## __str__ #####################
    def __str__(self):
############################################
        s = ""
        for i in self.galpos:
            s = s + str(i) + "\n"
        return s
