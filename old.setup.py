import sys,os,string,glob

from distutils.sysconfig import *
from distutils.core import setup,Extension
from distutils.command.build_ext import build_ext
from distutils.command.install import install
from distutils.command.install_data import install_data

import numpy

long_description = """\
This module provides lots of tools used
to deal with Gadget models.
"""

INCDIRS=['.']
SCRIPTS = glob.glob('scripts/*')

##############################################################
# variables for numpy
##############################################################
INCDIRS.append(numpy.get_include())
INCDIRS.append(numpy.get_numarray_include())

class Gtools_install_data (install_data):
  def finalize_options (self):
    if self.install_dir is None:
      install_lib = self.get_finalized_command('install_lib')
      self.warn_dir = 0
      self.install_dir = os.path.normpath(os.path.join(install_lib.install_dir,"Gtools"))
      

     

setup 	(       name	       	= "Gtools",
       		version         = "1.0.3",
       		author	      	= "Revaz Yves",
       		author_email    = "yves.revaz@obs.unige.ch",
		url 		= "http://obswww.unige.ch/~revaz",
       		description     = "Gtools module",
		
		packages 	= ['Gtools'],
		
		cmdclass = {'install_data': Gtools_install_data},
		
                ext_modules 	= [Extension("Gtools.cgtools", 
				             ["src/cgtools/cgtools.c"],
					     include_dirs=INCDIRS)
					   ],
				   
		data_files=[],
			    
                scripts = SCRIPTS			    
 	)
		
