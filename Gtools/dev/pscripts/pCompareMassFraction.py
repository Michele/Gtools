#!/usr/bin/env python

import numpy as np
import Ptools as pt
import pickle
import os

import matplotlib.mlab as ml

from optparse import OptionParser


############ parseArgument #################
def parseArgument():
############################################
    """
    Deal with the argument parser.
    :returns: files, opt
    """
    usage = """usage: %prog [options] file"""
    
    parser = OptionParser(usage=usage)

    nber_file = 1
    
    ptools_options = [
        "postscript",
        "limits",
        "log",
    ]

    parser.add_option("--x", 
                      action="store", 
                      dest="x",
                      default=False,
                      help="Value to plot on axis X")

    parser.add_option("--y",
                      action="store",
                      dest="y",
                      default=False,
                      help="Value to plot on axis Y")


    return pt.parse_options(
        usage=usage,
        options=ptools_options,
        parser=parser,
        nber_file=nber_file)

def getGasMassFraction(data):
    tmp = np.zeros(len(data))
    for i in range(len(data)):
        key = data.keys()[i]
        tmp[i] = data[key][1][-1] / data[key][1][0]
    return tmp

def getValue(val, data):
    print "NEED TO IMPROVE"
    for key in data.keys():
        if val not in key:
            del data[key]
    tmp = np.zeros(len(data))
    for i in range(len(data)):
        name = data.keys()[i]
        split_name = name.split("_")
        j = split_name.index(val)
        tmp[i] = split_name[j+1]
    return tmp

def getLabel(val):
    if val == "rho":
        return "Density NEED UNITS"
    elif val == "T":
        return "Temperature [K]"
    elif val == "u":
        return "Velocity [NEED UNITS]"
    else:
        raise Exception("Not Implemented")


def cleanDuplicate(x,y,z):
    N = len(x)
    out_xy = []
    out_z = []
    count = []
    for i in range(N):
        tmp = (x[i], y[i])
        if tmp in out_xy:
            j = out_xy.index(tmp)
            count[j] += 1
            out_z[j] += z[i]

        else:
            out_xy.append(tmp)
            out_z.append(z[i])
            count.append(1)

    out_z = np.array(out_z)
    count = np.array(count)
    
    out_z /= count

    out_xy = np.array(out_xy)
    out_x = out_xy[:,0]
    out_y = out_xy[:,1]
    return out_x, out_y, out_z


def MakePlot(f, opt):
    data = {}

    if os.path.isfile(f):
        f = open(f, "rb")
        data = pickle.load(f)
        f.close()
    else:
        raise Exception("File %s not found" % f)

    ########
    # plot
    ########
    
    # init plot
    colors = pt.Colors(n=len(data))
    plot_data = []
    
    # compute values
    x = getValue(opt.x, data)
    y = getValue(opt.y, data)
    z = getGasMassFraction(data)

    x, y, z = cleanDuplicate(x,y,z)
    tmp = pt.DataPoints(x, y, z, tpe="scatter")
    plot_data.append(tmp)

    x_mesh = np.linspace(x.min(), x.max(), 1000)
    y_mesh = np.linspace(y.min(), y.max(), 1000)
    z_mesh = ml.griddata(x, y, z, x_mesh, y_mesh)
    
    plt.contour(x_mesh, yi_mesh, zi_mesh, 15, linewidths = 0.5, colors = 'k')
    plt.pcolormesh(x_mesh, y_mesh, z_mesh, cmap = plt.get_cmap('rainbow'))

    # plot
    for d in plot_data:
        pt.scatter(d.x,d.y,c=d.z, s=100, marker='d')

    # set limits and draw axis
    xmin,xmax,ymin,ymax = pt.SetLimitsFromDataPoints(opt.xmin, opt.xmax, opt.ymin, opt.ymax,
                                                         plot_data, opt.log)

    # finalize plot
    xlabel = getLabel(opt.x)
    ylabel = getLabel(opt.y)
    
    pt.SetAxis(xmin,xmax,ymin,ymax,log=opt.log)
    pt.xlabel(xlabel,fontsize=pt.labelfont)
    pt.ylabel(ylabel,fontsize=pt.labelfont)
    
    pt.grid(False)

    pt.colorbar()

if __name__ == "__main__":
    files,opt = parseArgument()  
    f = files[0]
    pt.InitPlot(files,opt)
    pt.pcolors
    MakePlot(f,opt)
    pt.EndPlot(files,opt)
