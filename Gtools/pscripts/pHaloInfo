#!/dios/shared/apps/python/2.7.8/bin/python



from optparse import OptionParser
import Ptools as pt
from pNbody import *
from pNbody import units
from pNbody import ctes
from pNbody import thermodyn
import string
import copy
import pickle
import numpy as np

from scipy import optimize

def parse_options():


  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser = pt.add_postscript_options(parser)
  parser = pt.add_ftype_options(parser)
  parser = pt.add_reduc_options(parser)
  parser = pt.add_center_options(parser)
  parser = pt.add_select_options(parser)
  parser = pt.add_cmd_options(parser)
  parser = pt.add_display_options(parser)
  parser = pt.add_info_options(parser)
  parser = pt.add_limits_options(parser)
  parser = pt.add_log_options(parser)
  parse  = pt.add_units_options(parser)

  parser.add_option("--x",
                    action="store",
                    dest="x",
                    type="string",
                    default = 'r',
                    help="x value to plot",
                    metavar=" STRING")

  parser.add_option("--y",
                    action="store",
                    dest="y",
                    type="string",
                    default = 'T',
                    help="y value to plot",
                    metavar=" STRING")

  parser.add_option("--z",
                    action="store",
                    dest="z",
                    type="string",
                    default = None,
                    help="z value to plot",
                    metavar=" STRING")

  parser.add_option("--legend",
                    action="store_true",
                    dest="legend",
                    default = False,
                    help="add a legend")

  parser.add_option("--colorbar",
                    action="store_true",
                    dest="colorbar",
                    default = False,
                    help="add a colorbar")

  parser.add_option("--nx",
                    action="store",
                    type="int",
                    dest="nx",
                    default = 64,
                    help="number of bins in x")

  parser.add_option("--ny",
                    action="store",
                    type="int",
                    dest="ny",
                    default = 64,
                    help="number of bins in y")


  parser.add_option("--nopoints",
                    action="store_true",
                    dest="nopoints",
                    default = False,
                    help="do not plot points")

  parser.add_option("--map",
                    action="store_true",
                    dest="map",
                    default = False,
                    help="plot map instead of points")

  parser.add_option("--accumulate",
                    action="store_true",
                    dest="accumulate",
                    default = False,
                    help="integrate histogramm")

  parser.add_option("--density",
                    action="store_true",
                    dest="density",
                    default = False,
                    help="compute density")

  parser.add_option("--data",
                    action="store",
                    type="string",
                    dest="data",
                    default = None,
                    help="data")

  parser.add_option("--mc",
                    action="store",
                    type="int",
                    dest="mc",
                    default = 0,
                    help="number montecarlo point")


  parser.add_option("--forceComovingIntegrationOn",
                    action="store_true",
                    dest="forceComovingIntegrationOn",
                    default = False,
                    help="force the model to be in in comoving integration")

  parser.add_option("--pickle",
                    action="store",
                    type="string",
                    dest="pickle",
                    default = None,
                    help="FILE")

  parser.add_option("-o", "--output",
                    action="store",
                    type="string",
                    dest="output",
                    default = None,
                    help="FILE")


  parser.add_option("--HubbleParameter",
                    action="store",
                    type="float",
                    dest="HubbleParameter",
                    default = 1.0,
                    help="set HubbleParameter")



  (options, args) = parser.parse_args()


  pt.check_files_number(args)

  files = args

  return files,options






#######################################
# MakePlot
#######################################

angles = concatenate( ( arange(0,pi,pi/4.), arange(0,pi,pi/4.)[1:] ) )
axiss  = ['y','y','y','y','x','x','x']



def MakePlot(files,opt):


  if opt.pickle:
    print "reading data from %s"%opt.pickle
    fp = open(opt.pickle,'r')
    dic = pickle.load(fp)
    fp.close()
  elif opt.output:
    dic = {}

  # some inits
  colors     = pt.Colors(n=len(files))
  datas      = []


  # read files
  for file in files:







    print 100*"#"
    print
    print file
    print
    print 100*"#"


    mdict = {}

    # name
    bname = os.path.basename(file)
    bname,ext = os.path.splitext(bname)
    pName = r"$\rm{%s}$"%bname.replace('_', '\_')


    if opt.pickle:
      # check if file exists in the pickle file
      if dic.has_key(bname):
        mdict = dic[bname]
      else:
        print "%s does not exists in %s file..."%(bname,opt.pickle)
        sys.exit()






    nb = Nbody(file,ftype=opt.ftype)

    ################
    # units
    ################

    # define local units
    unit_params = pt.do_units_options(opt)
    nb.set_local_system_of_units(params=unit_params)

    # define output units
    # nb.ToPhysicalUnits()

    if opt.forceComovingIntegrationOn:
      nb.setComovingIntegrationOn()


    ################
    # apply options
    ################
    nb = pt.do_reduc_options(nb,opt)
    nb = pt.do_select_options(nb,opt)
    nb = pt.do_center_options(nb,opt)
    nb = pt.do_cmd_options(nb,opt)
    nb = pt.do_info_options(nb,opt)
    nb = pt.do_display_options(nb,opt)

    ################
    # some info
    ################
    print "---------------------------------------------------------"
    nb.localsystem_of_units.info()
    nb.ComovingIntegrationInfo()
    print "---------------------------------------------------------"


    ################
    # get values
    ################


    ######################################
    # compute time at a=1
    ######################################

    UnitTime_in_s=  unit_params['UnitLength_in_cm']/unit_params['UnitVelocity_in_cm_per_s']

    HUBBLE      = 3.2407789e-18	# Hubble param in h/sec
    Hubble = HUBBLE * UnitTime_in_s
    HubbleParam = unit_params['HubbleParam']

    params={}
    params['OmegaLambda'] = unit_params['OmegaLambda']
    params['Omega0']      = unit_params['Omega0']
    params['Hubble']      = Hubble

    tend = cosmo.CosmicTime_a(a=1,pars=params) / opt.HubbleParameter
    tend = tend[0]


    ######################################
    # number of particles
    ######################################

    Ngas   = nb.npart[0]
    Nstars = nb.npart[1]
    Ndark  = nb.npart[2]
    Nbndry = nb.npart[5]

    mdict["Ngas"]   = Ngas
    mdict["Nstars"] = Nstars
    mdict["Ndark"]  = Ndark
    mdict["Nbndry"] = Nbndry


    ######################################
    # compute R200, M200
    ######################################

    R200,M200 = nb.GetVirialRadius(omega0=unit_params['Omega0'])

    pR200= r"$R_{200}= %8.2f \,\rm{kpc}$"%R200
    pM200= r"$M_{200}= %8.2f \cdot 10^9 \,\rm{M_{\odot}}$"%(M200/1e9)

    mdict["R200"] = R200
    mdict["M200"] = M200

    nb200  = nb.selectc(nb.rxyz()<R200)

    nbg200 = nb200.select(0)
    nbs200 = nb200.select(1)
    nbd200 = nb200.select(2)
    nbb200 = nb200.select(5)


    ######################################
    # contamination in M200
    ######################################


    Contamination = nbb200.mass_tot/nb200.mass_tot
    mdict["Contamination"] = Contamination


    ######################################
    # baryonic fraction in M200
    ######################################

    M = nbg200.mass_tot + nbs200.mass_tot + nbd200.mass_tot

    BaryonFraction   = ((nbg200.mass_tot + nbs200.mass_tot )  / M )
    GasFraction      = ((nbg200.mass_tot                   )  / M )
    StarFraction     = ((                  nbs200.mass_tot )  / M )

    mdict["BaryonFraction"] = BaryonFraction
    mdict["GasFraction"]    = GasFraction
    mdict["StarFraction"]   = StarFraction



    ######################################
    # masses in M200
    ######################################


    out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Msol,units.Unit_s,units.Unit_K])
    fM = nb.localsystem_of_units.convertionFactorTo(out_units.UnitMass)

    Ms  = nbs200.mass_tot * fM
    Mg  = nbg200.mass_tot * fM
    Md  = nbd200.mass_tot * fM
    Mb  = nbb200.mass_tot * fM

    if nb.isComovingIntegrationOn():
      Ms  = Ms/HubbleParam
      Mg  = Mg/HubbleParam
      Md  = Md/HubbleParam
      Mb  = Mb/HubbleParam

    mdict["Ms"] = Ms
    mdict["Mg"] = Mg
    mdict["Md"] = Md
    mdict["Mb"] = Mb

    ###########################################
    # gas with temperature below 10000


    if nbg200.nbody>0:

      nbg200.Temperature = nbg200.T()

      nbgas4 = nbg200.selectc(nbg200.Temperature<1e4)
      nbgas2 = nbg200.selectc(nbg200.Temperature<1e2)


      Mg10000K  = nbgas4.mass_tot * fM
      Mg100K    = nbgas2.mass_tot * fM

      if nb.isComovingIntegrationOn():
        Mg10000K   = Mg10000K /HubbleParam
        Mg100K     = Mg100K   /HubbleParam

      mdict["Mg10000"] = Mg10000K
      mdict["Mg100"] = Mg100K

    else:

      mdict["Mg10000"] = 0
      mdict["Mg100"] = 0



    ######################################
    # compute luminosity and tidal radius
    ######################################

    nbs = nb.select(1)

    out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Msol,units.Unit_s,units.Unit_K])
    fM = nbs.localsystem_of_units.convertionFactorTo(out_units.UnitMass)
    Ms = nbs.mass_tot * fM
    pMs = r"$M_{s}= %8.2f \cdot 10^6 \,\rm{M_{\odot}}$"%(Ms/1e6)

    #mdict["Ms"] = Ms

    if nb.isComovingIntegrationOn():
      R200 = R200/nb.atime * HubbleParam

    nbs = nbs.selectc(nbs.rxyz()<R200)


    ############################
    # find tR using bissectrice
    ############################
    from scipy.optimize import bisect as bisection
    lmax=90

    nbs.L = nbs.Luminosity(tnow=None)
    Ltot = sum(nbs.L)
    rs       = nbs.rxyz()

    def getL(r):
      nb_s = nbs.selectc(rs<r)
      L    = sum(nb_s.L)
      return lmax - 100*L/Ltot

    Rt = bisection(getL, 0.0, 50, args = (), xtol = 0.1, maxiter = 400)

    nb_s = nbs.selectc(rs<Rt)
    L    = sum(nb_s.L)

    out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Msol,units.Unit_s,units.Unit_K])
    fL = nbs.localsystem_of_units.convertionFactorTo(out_units.UnitLength)


    mdict["Rt"] = Rt*fL/HubbleParam
    mdict["Lt"] = L
    mdict["LT"] = Ltot


    # compute final luminosity
    L    = sum(nb_s.Luminosity(tnow=tend))
    Ltot = sum(nbs.Luminosity(tnow=tend))

    mdict["Lt_end"] = L
    mdict["LT_end"] = Ltot


    ############################
    # Mass in Rt
    ############################

    nbs = nb.selectc(nb.rxyz()<Rt/HubbleParam)
    MRt = nbs.mass_tot*fM

    mdict["MRt"] = MRt



    ############################
    # find R12 using bissectrice
    ############################
    from scipy.optimize import bisect as bisection
    lmax=50

    nbs200.L = nbs200.Luminosity(tnow=None)
    Ltot = sum(nbs200.L)

    R12s = zeros(len(angles))
    L12s = zeros(len(angles))
    L12_ends = zeros(len(angles))


    # loop over different line of sight
    for i in xrange(len(angles)):

      nbss = copy.deepcopy(nbs200)

      angle = angles[i]
      axis  = axiss[i]

      nbss.rotate(angle=angle,axis=axis)


      rs       = nbss.rxy()

      def getL(r):
        nb_s = nbss.selectc(rs<r)
        L    = sum(nb_s.L)
        return lmax - 100*L/Ltot

      R12 = bisection(getL, 0.0, R200, args = (), xtol = 0.1, maxiter = 400)

      nb_s = nbss.selectc(rs<R12)
      L12  = sum(nb_s.L)

      out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Msol,units.Unit_s,units.Unit_K])
      fL = nbs.localsystem_of_units.convertionFactorTo(out_units.UnitLength)


      R12s[i] = R12*fL/HubbleParam
      L12s[i] = L12


      L12_ends[i] = sum(nb_s.Luminosity(tnow=tend))


    mdict["R12"]  = R12s.mean()
    mdict["L12"]  = L12s.mean()
    mdict["L12_end"]  = L12_ends.mean()

    mdict["ER12"] = R12s.std()
    mdict["EL12"] = L12s.std()
    mdict["EL12_end"] = L12_ends.std()




    ############################
    # Mass in R12
    ############################

    nbs = nb.selectc(nb.rxyz()<R12/HubbleParam)
    MR12 = nbs.mass_tot*fM

    mdict["MR12"] = MR12

    ############################
    # Mode Metallicity
    ############################


    nbs = nb.select(1)
    x = nbs.Fe()

    # do the 1d histogram
    G = libgrid.Generic_1d_Grid(-4,0.5,40)
    #y = G.get_MassMap(x,nb.mass)/sum(nb.mass)		# mass weighted
    y = G.get_MassMap(x,ones(nbs.nbody))/nbs.nbody
    x = G.get_r()

    i = argmax(y)
    Fe = x[i]

    pFe = r"$[\rm{Fe}/\rm{H}]   = %8.2f$"%(Fe)
    mdict["Fe"] = Fe





    ############################
    # Velocity Disp
    ############################

    out_units = units.UnitSystem('local',[units.Unit_km,units.Unit_Msol,units.Unit_s,units.Unit_K])
    fM = nbs.localsystem_of_units.convertionFactorTo(out_units.UnitVelocity)


    ##################
    # in 1 kpc

    nbs = nb.select(1)
    nbs = nbs.selectc(nbs.rxy()<1)
    sigmas = zeros(len(angles))



    # loop over different line of sight
    for i in xrange(len(angles)):

      nbss = copy.deepcopy(nbs)

      angle = angles[i]
      axis  = axiss[i]

      nbss.rotate(angle=angle,axis=axis)

      vz = nbss.vel[:,2]
      m  = nbss.mass
      vzm = sum(vz*m)/sum(m)

      sigmavz2 = sum(m*vz**2)/sum(m) - vzm**2
      sigmavz  = sqrt(sigmavz2) * fM

      sigmas[i] = sigmavz


    mdict["Sigma_zs"]  = sigmas
    mdict["Sigma_z"]  = sigmas.mean()
    mdict["ESigma_z"] = sigmas.std()



    ##################
    # in R12

    nbs = nb.select(1)
    nbs = nbs.selectc(nbs.rxy()<R12)

    sigmas = zeros(len(angles))


    # loop over different lines of sight
    for i in xrange(len(angles)):

      nbss = copy.deepcopy(nbs)

      angle = angles[i]
      axis  = axiss[i]

      nbss.rotate(angle=angle,axis=axis)

      vz = nbss.vel[:,2]
      m  = nbss.mass
      vzm = sum(vz*m)/sum(m)

      sigmavz2 = sum(m*vz**2)/sum(m) - vzm**2
      sigmavz  = sqrt(sigmavz2) * fM

      sigmas[i] = sigmavz

    mdict["Sigma_zs_R12"]  = sigmas
    mdict["Sigma_z_R12"]  = sigmas.mean()
    mdict["ESigma_z_R12"] = sigmas.std()



    ############################
    # Star formation
    ############################

    nbs = nb.select(1)
    xmin=0.0
    xmax=14.0
    nx = 500

    # cosmictime
    out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Ms,units.Unit_Gyr,units.Unit_K])
    CurrentTime = nb.CosmicTime(age=nb.atime,units=out_units.UnitTime)

    # do the 1d histogram
    G = libgrid.Generic_1d_Grid(xmin,xmax,nx)


    out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Ms,units.Unit_Gyr,units.Unit_K])
    x = nbs.CosmicTime(units=out_units.UnitTime)
    y = G.get_MassMap(x,nbs.mass)
    x = G.get_r()


    ##################
    # SFR


    y_out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Ms,units.Unit_yr,units.Unit_K])
    y = y * nb.localsystem_of_units.convertionFactorTo(units.Unit_Ms)
    dt = (x[1:]-x[:-1])*1e9 # Gyrs to yrs
    dMdt = y[:-1] / dt

    y = dMdt
    x = x[1:]

    # comoving conversion
    if nb.isComovingIntegrationOn():
      print "    converting to physical units (a=%5.3f h=%5.3f)"%(nb.atime,nb.hubbleparam)
      y = y/nb.hubbleparam                    # mass conversion

    data = [x,y]
    mdict['SFR'] = data


    ##################
    # MSTARS

    y_out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Ms,units.Unit_yr,units.Unit_K])
    y = y * nb.localsystem_of_units.convertionFactorTo(units.Unit_Ms)

    y = add.accumulate(y)

    # comoving conversion
    if nb.isComovingIntegrationOn():
      print "    converting to physical units (a=%5.3f h=%5.3f)"%(nb.atime,nb.hubbleparam)
      y = y/nb.hubbleparam                    # mass conversion


    data = [x,y]
    mdict['MSTARS'] = data


    ############################
    # Velocities
    ############################


    # output units
    out_units_x = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Ms,units.Unit_Myr,units.Unit_K])
    out_units_y = units.UnitSystem('local',[units.Unit_km,units.Unit_Ms,units.Unit_s,units.Unit_K])

    fx = nb.localsystem_of_units.convertionFactorTo(out_units_x.UnitLength)
    fy = nb.localsystem_of_units.convertionFactorTo(out_units_y.UnitVelocity)


    #######################
    # circular velocity

    rmax = 10.
    nr   = 64


    # grid division
    rc = 1
    f     = lambda r:log(r/rc+1.)
    fm    = lambda r:rc*(exp(r)-1.)


    Gcte = ctes.GRAVITY.into(nb.localsystem_of_units)

    G = libgrid.Spherical_1d_Grid(rmin=0,rmax=rmax,nr=nr,g=f,gm=fm)

    x  = G.get_r()
    M  = G.get_MassMap(nb)
    M = add.accumulate(M)

    y = sqrt(Gcte * M/x)

    # comoving conversion
    if nb.isComovingIntegrationOn():
      print "    converting to physical units (a=%5.3f h=%5.3f)"%(nb.atime,nb.hubbleparam)
      x = x*nb.atime/nb.hubbleparam           # length  conversion


    x = x*fx
    y = y*fy


    data = [x,y]
    mdict['Vcirc'] = data



    #######################
    # velocity disp. in z (stars)

    rmax = Rt
    nr   = 12

    nbs = nb.select('stars1')

    # comoving conversion
    if nbs.isComovingIntegrationOn():
      print "    converting to physical units (a=%5.3f h=%5.3f)"%(nbs.atime,nbs.hubbleparam)

      x0 = nbs.pos
      v0 = nbs.vel

      Hubble = ctes.HUBBLE.into(nbs.localsystem_of_units)
      pars = {"Hubble":Hubble,"HubbleParam":nbs.hubbleparam,"OmegaLambda":nbs.omegalambda,"Omega0":nbs.omega0}
      a  = nbs.atime
      Ha = cosmo.Hubble_a(a,pars=pars)

      nbs.pos = x0*nbs.atime/nbs.hubbleparam     # length    conversion
      nbs.vel = v0*sqrt(a) + x0*Ha*a          # velocity  conversion


    nbs.pos = nbs.pos*fx
    nbs.vel = nbs.vel*fy


    G = libgrid.Cylindrical_2drt_Grid(rmin=0,rmax=rmax,nr=nr,nt=1,g=f,gm=fm)

    x,t  = G.get_rt()

    y1s = zeros(nr)
    y2s = zeros(nr)
    ns  = zeros(nr)

    # loop over different line of sight
    for i in xrange(len(angles)):

      nbsr = copy.deepcopy(nbs)

      angle = angles[i]
      axis  = axiss[i]

      nbsr.rotate(angle=angle,axis=axis)


      y  = G.get_SigmaValMap(nbsr,nbsr.Vz())
      y = sum(y,axis=1)

      #x,y = pt.CleanVectorsForLogX(x,y)
      #x,y = pt.CleanVectorsForLogY(x,y)

      y1s = y1s + y
      y2s = y2s + y*y
      ns = ns   + where(y>0,1,0)


    # remove odd values
    c = ns > 0
    y1s = compress(c,y1s)
    y2s = compress(c,y2s)
    ns  = compress(c,ns)
    x   = compress(c,x)

    # compute the mean
    ym = y1s / ns
    # compute the std
    Ey = sqrt(  y2s/ns  - (ym**2)  )





    data = [x,ym,Ey]
    mdict['SigmaLOS'] = data



    ############################
    # Time
    ############################

    out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Ms,units.Unit_Gyr,units.Unit_K])
    CurrentTime = nb.CosmicTime(age=nb.atime,units=out_units.UnitTime)


    pT= r"$t    = %8.2f \,\rm{Gyr}$"%CurrentTime

    mdict["Time"] = CurrentTime




    ############################
    # Fe Lv evolution
    ############################


    nb = nb.select(1)



    # compute age for all particles

    out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Ms,units.Unit_Gyr,units.Unit_K])
    nb.Ages = nb.StellarAge(units=out_units.UnitTime)
    nb.L = nb.Luminosity(tnow=tend)

    dt = 0.25 # gyr
    ts = arange(0,14+dt,dt)


    Ages = []
    Lvs  = []
    FeHs = []
    FeHms = []



    for i in xrange(len(ts)-1):
      t1 = ts[i]
      t2 = ts[i+1]

      t = 0.5*(t1+t2)


      ##############################
      # select for Luminosity
      ##############################

      c = (nb.Ages>=t) #* (Ages<=tmax)
      nbs = nb.selectc(c)

      # compute the luminosity
      #nbs.L = nbs.Luminosity(tnow=1.0)
      L = sum(nbs.L)


      Ages.append(t)
      Lvs.append( L )

      ##############################
      # select for Metallicity
      ##############################


      #c = (t1 < nb.Ages) * (nb.Ages < t2) * (nb.Fe()>-20)
      c = (nb.Ages>=t) * (nb.Fe()>-20)

      nbs = nb.selectc(c)


      if nbs.nbody > 0:

        # compute the mode
        x = nbs.Fe()

        # do the 1d histogram
        G = libgrid.Generic_1d_Grid(-4,0.5,40)
        #y = G.get_MassMap(x,nbs.mass)/sum(nbs.mass)        # mass weighted
        y = G.get_MassMap(x,ones(nbs.nbody))/nbs.nbody
        x = G.get_r()

        Fe_mode = x[argmax(y)]
        Fe_mean = nbs.Fe().mean()

      else:
        Fe_mode = -20
        Fe_mean = -20

      FeHs.append(Fe_mode)
      FeHms.append(Fe_mean)





    Ages = array(Ages)
    Lvs = array(Lvs)
    FeHs = array(FeHs)
    FeHms = array(FeHms)


    #pt.plot(Lvs,FeHs)
    #pt.plot(Lvs,FeHms)
    #pt.semilogx()
    #pt.show()


    mdict["Ages"]  = Ages
    mdict["Lvs"]   = Lvs
    mdict["FeHs"]  = FeHs
    mdict["FeHms"] = FeHms











    ############################
    # save pickle file
    ############################


    if opt.pickle or opt.output:
      tmp = opt.pickle
      if opt.output:
        tmp = opt.output

      dic[bname]  = mdict

      fp = open(tmp,'w')
      pickle.dump(dic,fp)
      fp.close()




########################################################################
# MAIN
########################################################################



if __name__ == '__main__':
  files,opt = parse_options()
  pt.InitPlot(files,opt)
  pt.pcolors
  MakePlot(files,opt)
  #pt.EndPlot(files,opt)

