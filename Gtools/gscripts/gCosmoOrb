#!/usr/bin/env python
"""
@package   GTools
@file      gCosmoOrb.py
@brief     Transform a snapshot for a simulation with an external potential/halo
@copyright GPLv3
@author    Matthew Nichols, Loic Hausammann <loic_hausammann@hotmail.com>
@section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL
"""

'''
Transforms a snapshot into one suitable for insertion using the rotating potential.
Usage:
Boxsize = Size of box in output units
rdwarf = Size of dwarf in output units
ra = apogalacticon (output)
rp = perigalacticon
dir = Directory of potential files to integrate the orbit
r_pos = Present day radial position
rvsign = +ve if radial velocity is increasing, negative otherwise
mass = mass of the inserted gas particles, if negative the maximum gas mass in the snapshot is used
o = output file name
params = Input parameter files
paramout = Output parameter file where it puts extra information, if this does not exist the units should be the same between the two snapshots...
glass = location of glass files
t = time in Gyr to integrate back for
'''
#e.g. ./cosmo-orb.py -boxsize 60 -rdwarf 30 -ra 200 -rp 50 -r_pos 100 -rvsign -1 -t 11 -o snap.dat ../init_snap/dSphZoom_003_l9_halo00000021_v19102016_snapshot_0092_sub002.udSph --param ../test_cosmo/params -dir NFW_Potential

import scipy as sp
import numpy as np
import scipy.integrate
import scipy.interpolate
import sys
import os
from optparse import OptionParser
from pNbody import *
from pNbody import ic
from Gtools import io
from Gtools.orbitalIntegration import *
import shutil
import Ptools as pt

#################################
def writefile(opt, nb, denscale, rho_gas, m, endpt, ref_pot):
#################################
    """
    Write Snapshot and print params values.
    Opt should contains: output filename.
    :param ArgumentParser opt: Parser options
    :param Nbody nb: Nbody snapshot
    :param float denscale: Density scale
    :param float rho_gas: Density of gas
    :param float m: Gas particles mass
    :param list endpt: Final position on the orbit
    """
    if opt.outputfile!=None:
        nb.rename(opt.outputfile)
        nb.massarr = None
        nb.nzero = None
        nb.write()
    # [r, theta, v_r, v_theta]
    posx = endpt[0]*np.cos(endpt[1])
    posy = endpt[0]*np.sin(endpt[1])
    drx = endpt[2]*np.cos(endpt[1])
    dry = endpt[2]*np.sin(endpt[1])
    dthx = -endpt[3]*np.sin(endpt[1])
    dthy = endpt[3]*np.cos(endpt[1])
    velx = drx + dthx
    vely = dry + dthy

    print "#"*15
    print "Value for a full simulation"
    print "#"*15
    print "TracePosX ", posx
    print "TracePosY ", posy
    print "TraceVelX ", velx
    print "TraceVelY ", vely
    if opt.gas:
        print "HaloScale ", denscale
        print "HaloRefPot ", ref_pot

    if opt.add_glass:
        print "#"*15
        print "Value for a wind tunnel simulation"
        print "#"*15
        print "HaloVel ", np.sqrt(velx**2 + vely**2)
        print "HaloDensity ", rho_gas
    if opt.gas or opt.add_glass:
        print "HaloPMass ", m*1e10

#################################
def addPNbody(nb1, nb2):        
#################################
    """
    Add two Nbody objects while conserving IDs.
    :param Nbody nb1: Nbody object to add
    :param Nbody nb2: Nbody object to add
    :returns: Nbody (nb1 + nb2)
    """
    # first copy self
    new = deepcopy(nb1)
    # now, add solf    
    new.append(nb2, False, False)
    return new

#################################
def createHaloGas(dens, V, m, q, opt):
#################################
    """
    Create a Nbody object containing the gas at q in the halo.
    Opt should contains: boxsize, glass (filename to a glass file),
    offset, UL, UM, UT (Length, Mass and Temperature in cgs, gamma and T)
    :param float dens: Density at current position
    :param float V: Box volume
    :param float m: Gas particles mass
    :param list q: Generalized position [r,theta,v_r,d\theta/dt]
    :param ArgumentParser opt: Options
    :returns: Nbody containing the gas
    """
    N      = int(dens*V/m)

    glass = Nbody(opt.glass,ftype='gadget')
    bound = (N/(size(glass.mass)+0.))**(1./3)

    while bound > 1:
        for i in range(3):
            glass.rename('.temp.dat')
            glass.write()
            glass2 = Nbody('.temp.dat',ftype='gadget')
            glass2.pos[:,i] += 1.
            glass = glass+glass2
            glass.pos[:,i] /= 2.

        bound = (N/(float(size(glass.mass))))**(1./3)

    for i in range(3):
        glass = glass.selectc(glass.pos[:,i]<bound)

    glass.pos *= opt.boxsize/bound
    glass.pos -= opt.boxsize/2.
    N = size(glass.mass) #change N to better fit glass, won't have much impact due to near uniform distribution


    nb2 = ic.box(N,opt.boxsize/2.,opt.boxsize/2.,opt.boxsize/2.,ftype='gadget')
    nb2.pos = glass.pos

    nb2.mass = np.ones(nb2.nbody)*m
    nb2 = nb2.selectc(nb2.rxyz(center=[0,opt.offset,0])>=3)

    if 'rho' in nb1.get_list_of_array():
        nb2.rho    = np.ones(nb2.nbody).astype(float32)*dens

    if 'u' in nb1.get_list_of_array():
        # 1./(5./3-1)*k_b*opt.T/(opt.UM*opt.UL**2/opt.UT**2)/(amu/opt.UM) *
        pars = {}
        pars['k'] = k_b
        pars['mh'] = amu
        pars['mu'] = thermodyn.MeanWeightT(opt.T)
        pars['gamma'] = opt.gamma
        units = opt.UL**2 / opt.UT**2
        nb2.u      =  np.ones(nb2.nbody).astype(float32)*thermodyn.Urt(dens,opt.T,pars=pars) / units

    nb2.vel[:,1] = nb2.vel[:,1] + np.sqrt(q[2]**2+q[3]**2)
    return nb2

#################################
def addChemistry(nb, nb1):
#################################
    """
    Add the chemistry arrays present in nb1 to nb
    :param Nbody nb: Nbody to modify
    :param Nbody nb1: Nbody to copy
    :returns: Improved nb
    """
    NELEMENTS = nb1.NELEMENTS
    if NELEMENTS:
        nb.metals = np.zeros((nb.nbody,NELEMENTS)).astype(float32)
        nb.rsp    = np.zeros(nb.nbody).astype(float32)

    for i in nb1.get_list_of_array():
        if i not in nb.get_list_of_array():
            if i == 'idp': #Could be done more elegantly
                nb.idp    = np.zeros(nb.nbody).astype(int32)
            elif i == 'minit':
                nb.minit  = np.zeros(nb.nbody).astype(float32)
            elif i == 'tstar':
                nb.tstar  = np.zeros(nb.nbody).astype(float32)
            elif i == 'rsp':
                nb.rsp    = np.zeros(nb.nbody).astype(float32)
            elif i == 'opt1':
                nb.opt1 = np.zeros(nb.nbody).astype(float32)
            elif i == 'opt2':
                nb.opt2 = np.zeros(nb.nbody).astype(float32)
    return nb, NELEMENTS

#################################
def createAccretionGlass(filename, nb):
#################################
    """
    Create a Nbody object contaning glass particles in a unit box.
    :param string filename: Filename to the glass file
    :param Nbody nb: Nbody struct to copy
    :returns Nbody object containing the glass particles
    """
    glass = Nbody(opt.glass,ftype='gadget')
    glass.set_tpe(2)

    glass.mass = np.ones(glass.nbody)*-1
    glass.mass = glass.mass.astype(float32)

    arrays = glass.get_list_of_array()
    for i in nb.get_list_of_array():
        if i not in glass.get_list_of_array():
            if i == 'idp': #Could be done more elegantly
                glass.idp    = np.zeros(glass.nbody).astype(int32)
            elif i == 'metals':
                glass.metals = np.zeros((glass.nbody,nb.ChimieNelements)).astype(float32)
            else:
                setattr(glass, i, np.zeros(glass.nbody).astype(float32))

    glass.pos[:,1] = 1 - glass.pos[:,1]
    return glass

#################################
def parseOptions():
#################################
    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    # Ptools functions
    pt.add_ftype_options(parser)
    pt.add_units_options(parser)

    parser.add_option("-d", "--dir", 
                        action="store", 
                        dest="dir",
                        default="potential",
                        help="Potential directory", 
                        metavar="STRING")

    parser.add_option("--ra",
                        action="store",
                        type=float,
                        dest="ra",
                        help="Apogalacticon",
                        metavar="FLOAT")

    parser.add_option("--rp",
                        action="store",
                        type=float,
                        dest="rp",
                        help="Perigalacticon",
                        metavar="FLOAT")

    parser.add_option("--r_pos",
                        action="store",
                        type=float,
                        dest="r_pos",
                        help="Current Radius",
                        metavar="FLOAT")

    parser.add_option("--rvsign",
                        action="store",
                        type=int,
                        dest="rvsign",
                        help="Sign of the radial velocity",
                        default=-1,
                        metavar="INT")

    parser.add_option("--time",
                        action="store",
                        type=float,
                        dest="time",
                        help="Time to integrate back to in Gyr",
                        default=-1,
                        metavar="FLOAT")

    parser.add_option("--gas",
                        action="store",
                        dest="gas",
                        type=int,
                        help="Add gas to the halo",
                        default=1,
                        metavar="INT")

    parser.add_option("--ionized",
                        action="store",
                        dest="ionized",
                        type=int,
                        help="Ionized gas (if not specify find ionized fraction from temperature)",
                        default=None,
                        metavar="INT")

    parser.add_option("--add_glass",
                        action="store_true",
                        dest="add_glass",
                        help="In the case without gas, add the glass as if it was with gas")
    
    parser.add_option("-T", "--temperature",
                      action="store",
                      dest="T",
                      default=2e6,
                      help="Gas temperature",
                      metavar="FLOAT")

    parser.add_option("--gamma",
                      action="store",
                      dest="gamma",
                      default=5./3.,
                      help="Ideal gas gamma",
                      metavar="FLOAT")

    parser.add_option("--ne",
                      action="store",
                      dest="ne",
                      default=1e-4,
                      help="Electron density at 50kpc",
                      metavar="FLOAT")
    
    parser.add_option("-o", 
                        "--outputfile", 
		        action="store", 
		        dest="outputfile",
		        default="snap.dat",
                        help="output file name", 
		        metavar="FILE")

    parser.add_option("--ctype",
                      action="store",
                      type=int,
                      dest="ctype",
                      default=1,
                      help="0: does nothing, 1: use cmcenter, 2: use histocenter",
                      metavar="INT")
    
    parser.add_option("--rdwarf", 
		        action="store", 
		        type=float,
		        dest="rdwarf",
		        default=0.,
                        help="dwarf radius", 
		        metavar="FLOAT")

    parser.add_option("--offset", 
		        action="store", 
		        type=float,
		        dest="offset",
		        default=0.,
                        help="boxsize", 
		        metavar="FLOAT")

    parser.add_option("--boxsize", 
		        action="store", 
		        type=float,
		        dest="boxsize",
		        default=50.,
                        help="boxsize", 
		        metavar="FLOAT")
    
    parser.add_option("-m", "--mass", 
		        action="store", 
		        type=float,
		        dest="mass",
		        default=-1,
                        help="Mass of gas particles (gadget units), if negative, use the maximum", 
		        metavar="FLOAT")

    parser.add_option("--remove_gas", 
		        action="store_true", 
		        dest="remove_gas",
		        default=False,
                        help="Remove halo gas situated inside the galaxy")

    parser.add_option("--paramout",
                        action="store",
                        type=str,
                        dest="paramout",
                        default=None,
                        help="Output parameter file",
                        metavar="STRING")

    parser.add_option("--glass",
                        action="store",
                        type=str,
                        dest="glass",
                        default='/home/epfl/mlnichol/glass/snap/snapshot_0022',
                        help="Glass location",
                        metavar="STRING")
    
    parser.add_option("--orbit",
                        action="store",
                        dest="orbit",
                        default=None,
                        help="Orbit Filename",
                        metavar="STR")


    return parser




#################################
#################################
if __name__ == "__main__":
#################################
#################################
    """
    Create a snapshot containing particles from a hot halo from
    a list of potential files and current position in the halo.
    """

    # parse option
    opt, file = parseOptions().parse_args()
    if len(file) > 1:
        print "Please provide only one file"
        exit(123322)
    #Use the output param file if possible
    if opt.paramout != None:
        Param = io.read_params(opt.paramout)
    else:
        Param = io.read_params(opt.GadgetParameterFile)

    print "Taking time and units from params file..."
    t_init = Param["TimeMax"]

    opt.UL = Param['UnitLength_in_cm']
    opt.UV = Param['UnitVelocity_in_cm_per_s']
    opt.UT = opt.UL/opt.UV
    opt.UM = Param['UnitMass_in_g']

    # files containing informations about potential
    files = []
    for (dirpath, dirnames, filenames) in os.walk(opt.dir):
        files.extend(filenames)

    # get all the potentials
    potentials = [Potential(opt.dir+'/'+i) for i in files]

    # change units kpc -> cm
    ra = opt.ra * kpc
    rp = opt.rp * kpc
    r_pos = opt.r_pos * kpc
    # open file
    nb1 = Nbody(file,ftype=opt.ftype)
    if opt.remove_gas:
        ids = nb1.num
    # assure a null average velocity of the galaxy
    nb1.cvcenter()
    # if snapshot time not given, use the one from it
    if opt.time == -1:
        print "Getting time from snapshot..."
        opt.time = nb1.atime

    print "Final Time (backward integration): ", opt.time

    endpt = computeOrbit(ra, rp, r_pos, t_init, opt, potentials)

    rho_gas, density_scale, ref_pot = getHaloDensity(endpt[0], opt.time, opt.ne, opt.T, potentials, h_frac=0.76, opt=opt, today=t_init, ionized=opt.ionized)


    # change units to params ones
    endpt[3] *= endpt[0]
    endpt[0] /= opt.UL
    endpt[2] /= opt.UV
    endpt[3] /= opt.UV


    ################################
    # main
    ################################

    V      = opt.boxsize**3
    rdwarf = opt.rdwarf


    if opt.paramout != None:
        Param_in = io.read_params(opt.GadgetParameterFile)
        UL_in = Param['UnitLength_in_cm']
        UV_in = Param['UnitVelocity_in_cm_per_s']
        UT_in = opt.UL/opt.UV
        UM_in = Param['UnitMass_in_g']
    
        nb1.mass *= UM_in/opt.UM
        nb1.vel *= UV_in/opt.UV
        nb1.pos *= UL_in/opt.UL
        nb1.atime *= UT_in/opt.UT
        nb1.tstar *= UT_in/opt.UT

    # center galaxy
    if opt.ctype == 1:
        nb1.cmcenter()
    if opt.ctype == 2:
        nb1.histocenter()

    # select particles inside the dwarf radius
    for i in range(3):
        nb1 = nb1.selectc(np.sum(nb1.pos**2, axis=1) < rdwarf**2)

    nb1.rebox(mode=[0,0,0])
    nb1.translate([0,opt.offset,0])

    # mass of gas particles
    if opt.mass < 0:
        m = nb1.select(0).mass.max()
    else:
        m = opt.mass

    # if gas not wanted -> save snapshot and exit cosmo-orb
    if not opt.gas or opt.boxsize < 0:
        if opt.boxsize < 0:
            temp = deepcopy(endpt)
            # goes back to dtheta and v_theta
            temp[0] *= opt.UL
            temp[2] *= opt.UV
            temp[3] *= opt.UV
            temp[3] /= temp[0]
            pos = Cylindrical2Cartesian(temp, opt)
            cm = [pos[0], pos[1], -nb1.cm()[2]]
            nb1.translate(cm)
            nb1.vel += [pos[2], pos[3], 0]
        else:
            nb1.translate([opt.boxsize/2.,opt.boxsize/2.,opt.boxsize/2.])
        if opt.add_glass:
            glass = createAccretionGlass(opt.glass, nb1)
            glass = glass.set_ftype(nb1.ftype)
            nb1 = addPNbody(nb1, glass)
            nb1.vel = nb1.vel.astype(float32)
        writefile(opt, nb1, density_scale, rho_gas, m, endpt, ref_pot)
        exit()

    ############################################
    # this is the gas in the box
    ############################################

    nb = createHaloGas(rho_gas, V, m, endpt, opt)

    ############################################
    # add chemistry
    ############################################
    nb.rename('.qq.dat')
    nb.write()

    nb, NELEMENTS = addChemistry(nb, nb1)
    
    nb = addPNbody(nb1,nb)

    nb.translate([opt.boxsize/2.,opt.boxsize/2.,opt.boxsize/2.])


    ############################################
    # Add accretion Glass
    ############################################

    glass = createAccretionGlass(opt.glass, nb)

    nb = addPNbody(nb, glass)
    nb.vel = nb.vel.astype(float32)
    if opt.remove_gas:
        print "Removing host gas inside satellite galaxy"
        pos = nb.pos - [opt.boxsize/2]*3
        tmp = nb.selectc(np.sum(pos**2, axis=1) < opt.rdwarf**2)
        tmp = tmp.selectp(ids, reject=True)
        nb = nb.selectp(tmp.num, reject=True)
    writefile(opt, nb, density_scale, rho_gas, m, endpt, ref_pot)

    if opt.paramout != None:
        shutil.copy(opt.GadgetParameterFile,opt.paramout)
        with open(opt.paramout,'a') as pfile:
            pfile.write("%Trace Particle\n");
            pfile.write("TracePosX "+str(endpt[0])+"\n")
            pfile.write("TracePosY 0.\n")
            pfile.write("TracePosZ 0.\n")
            pfile.write("\nTraceVelX "+str(endpt[2])+"\n")
            pfile.write("TraceVelY "+str(endpt[3])+"\n")
            pfile.write("TraceVelZ 0.\n")
            pfile.write("\nPotentialDir "+opt.dir+"\n")
            pfile.write("\nInertialV 1\n")
            pfile.write("%Halo Properties\n")
            pfile.write("HaloT 2e6\n")
            pfile.write("HaloPMass "+str(m*1e10)+"\n")
            pfile.write("HaloScale "+str(density_scale)+"\n")
    
