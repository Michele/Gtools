#!/home/revaz/local/bin/python
'''
Plot star formation rate of the model as a function of time

Yves Revaz
ven mai 19 10:55:04 CEST 2006
'''


from numarray import *
from pNbody import *
from pNbody import cosmo
import SM
import string
import sys
import os

from libjeans import *

from pNbody.libutil import histogram 

from optparse import OptionParser

from Gtools import *
from Gtools import vanderwaals as vw




def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser = add_postscript_options(parser)
  parser = add_color_options(parser)
  parser = add_limits_options(parser)
  parser = add_units_options(parser)
  parser = add_log_options(parser)

  parser.add_option("-t",
		   action="store", 
		   dest="ftype",
		   type="string",
		   default = None,		   
		   help="type of the file",	 
		   metavar=" TYPE")    

  parser.add_option("--cgs",
  		    action="store_true", 
  		    dest="cgs",
		    default = 0,		    
  		    help="invert into cgs units")	

  parser.add_option("-r",
		   action="store", 
		   dest="reduc",
		   type="int",
		   default = 1,		   
		   help="reduction number",	 
		   metavar=" INT")    
		   
		    		    		    
  (options, args) = parser.parse_args()

  if options.colors!=None:
    exec("options.colors = array([%s])"%(options.colors)) 

        
  if len(args) == 0:
    print "you must specify a filename"
    sys.exit(0)
    
  files = args
  
  return files,options



#############################
# graph
#############################

# get options
files,options = parse_options()
ps = options.ps
col = options.colors
xmin = options.xmin
xmax = options.xmax
ymin = options.ymin
ymax = options.ymax
log = options.log

cgs  = options.cgs
ftype = options.ftype
reduc = options.reduc

localsystem = Set_SystemUnits_From_Options(options)


#######################################
# define output system of unit
#######################################

outputunits = units.UnitSystem('mks',[units.Unit_kpc,  units.Unit_Ms, units.Unit_yr  , units.Unit_K, units.Unit_mol, units.Unit_C])


#######################################
# open sm
#######################################

g = Graph_Init(ps)
Graph_SetDefaultsGraphSettings(g)
colors = Graph_SetColorsForFiles(files,col)


#######################################
# LOOP
#######################################

# read files
for file in files:
				     		
  try:
    vals = io.read_new_energy(file) 
  except "NotNewEgyFileError":  
    vals = io.read_energy(file,iobs=None)

  t = vals['Time']
  m = vals['MassComp5']
  
  # reduc
  n = reduc
  c = fmod(arange(len(t)),n)
  t = compress((c==0),t)
  m = compress((c==0),m) 
    
  dt  = (t[1:] - t[0:-1])
  sfr = (m[1:] - m[0:-1])/dt
  
  x = t[1:]
  y = sfr
      
  # sfr factor
  FACT = ctes.PhysCte(1,localsystem.UnitDic['kg']/localsystem.UnitDic['s'])
  FACT = FACT.into(outputunits)
  
  # mass factor
  #FACT = PhysCte(1,localsystem.UnitDic['kg'])		
  #FACT = FACT.into(outputunits)

  y = y*FACT
  
  
  # use log
  if log != None:
    x,y = Graph_UseLog(x,y,log)
  
  if file == files[0]:
    xmin,xmax,ymin,ymax = Graph_SetLimits(g,xmin,xmax,ymin,ymax,x,y)
    g.box()
    			   
  
  # plot points	
  g.ctype(colors[file])	
  g.connect(x,y)



g.ctype(0)
if   log == 'xy' or log == 'yx':
  g.xlabel('log time')
  g.ylabel('log SFR')
elif log == 'x':
  g.xlabel('log time')
  g.ylabel('SFR')
elif log == 'y':  
  g.xlabel('time')
  g.ylabel('log SFR')
else:
  g.xlabel('time')
  g.ylabel('SFR')


# -- end ---
Graph_End(g,ps)












