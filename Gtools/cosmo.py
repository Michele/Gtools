from numpy import *
from pNbody.ctes import *
from pNbody import units
import cgtools

####################################################################################################################################
#
# THERMODYNAMIC RELATIONS
#
####################################################################################################################################    

# deflauts cosmo parameters
defaultpars = {"Hubble":0.1,"OmegaLambda":0.76,"Omega0":0.24}



###################
def Z_a(a):
###################
  '''
  z(a)
  '''
  return 1./a - 1

###################
def A_z(z):
###################
  '''
  a(z)
  '''
  return 1/(z+1)

###################
def Hubble_a(a,pars=defaultpars):
###################
  '''
  H(a)
  '''
  OmegaLambda	= pars['OmegaLambda']
  Omega0	= pars['Omega0']
  Hubble	= pars['Hubble']

  hubble_a = Omega0 / (a * a * a) + (1 - Omega0 - OmegaLambda) / (a * a) + OmegaLambda
  hubble_a = Hubble * sqrt(hubble_a)

  return hubble_a


###################
def Adot_a(a,pars=defaultpars):
###################
  '''
  da/dt
  '''
  OmegaLambda	= pars['OmegaLambda']
  Omega0	= pars['Omega0']
  Hubble	= pars['Hubble']

  hubble_a = Omega0 / (a * a * a) + (1 - Omega0 - OmegaLambda) / (a * a) + OmegaLambda
  hubble_a = Hubble * sqrt(hubble_a)
  adot_a   = hubble_a*a

  return adot_a
 
###################
def Age_a(a,pars=defaultpars):
###################
  '''
  cosmic age as a function of a
  '''
  OmegaLambda	= pars['OmegaLambda']
  Omega0	= pars['Omega0']
  Hubble	= pars['Hubble']
  
  a = array([a],float)
  LITTLEH = 0.7
  age_a = cgtools.Age_a(a,Omega0,OmegaLambda,Hubble)  *0.978/LITTLEH
 
 

  return age_a
 
